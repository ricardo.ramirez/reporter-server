package py.com.cloudsolutions.vetpet.reporterserver.util;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import py.com.cloudsolutions.vetpet.reporterserver.annotations.ReportEngine;
import py.com.cloudsolutions.vetpet.reporterserver.report.BirtReportEngine;

@ApplicationScoped
public class Resources {

	@Produces
	public Logger produceLog(InjectionPoint injectionPoint) {
		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
	}

	@Inject
	@ReportEngine
	private BirtReportEngine birtReportEngine;

	@Produces
	@ReportEngine
	@ApplicationScoped
	public BirtReportEngine createBirtReportEngine() {
		Logger logger = Logger.getLogger(Resources.class.getName());
		logger.info("Birt engine started");
		return this.birtReportEngine;
	}

	public void closeReportEngine(@Disposes @ReportEngine BirtReportEngine engine) {
		Logger logger = Logger.getLogger(Resources.class.getName());
		logger.info("Birt engine stoped");
		engine.destroyBirtEngine();
	}

}
